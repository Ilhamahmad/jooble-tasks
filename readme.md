# Hi!

# Step 1
For running project you need to install requirements. For this use below code:
```
pip install -r requirements.txt
```

# Step 2
After installing packages use following commands for running project: 
```
uvicorn main:app --reload
```
>Default port is 8000
using `--port 8001` you can change port number

After finising steps you can go browser and goto ' localhost:8000/docs '
