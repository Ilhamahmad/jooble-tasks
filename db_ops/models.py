from .database import Base
from sqlalchemy import Column, String, Integer


class Addresses(Base):
    __tablename__ = "adresses"

    id = Column(Integer, primary_key=True, index=True)
    url = Column(String)
    final_url = Column(String, nullable=True)
    title = Column(String)
    domain_name = Column(String)
    status_code = Column(Integer)
