from pydantic import BaseModel


class Addresses(BaseModel):
    final_url: str | None = None
    title: str | None = None
    domain_name: str
    status_code: int

    class Config:
        orm_mode = True


class Pages(BaseModel):
    active_page_count: int
    page_count: int
    url_list: list[str] = []

    class Config:
        orm_mode = True
