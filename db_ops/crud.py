from . import models
from sqlalchemy.orm import Session


def create_address(db: Session, address):
    db_address = models.Addresses(**address)
    db.add(db_address)
    db.commit()
    db.refresh(db_address)
    return db_address


def get_all_addresses(db: Session):
    addresses = db.query(models.Addresses).all()
    return addresses
