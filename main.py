from fastapi import FastAPI, status, Depends, Body, HTTPException
from db_ops import schemas, models, crud
from db_ops.database import engine, SessionLocal
from bs4 import BeautifulSoup
import requests

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

url_links = []


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def get_text(text: str, start, end):
    if text.__contains__(start):
        return (text.split(start))[1].split(end)[0]
    else:
        return ''


def get_pages(url):
    html_data = requests.get(url).text
    soup = BeautifulSoup(html_data, features="html.parser")
    links = []
    for link in soup.find_all("a", href=True):
        if link.get("href").startswith(url):
            links.append(link.get('href'))

        if link.get("href").startswith("/") and link.get('href') not in url_links:
            url_links.append(link.get("href"))
            full_link = url + link.get("href")[1:]
            links.append(full_link)
    dict_links = dict.fromkeys(links, True)
    return dict_links


def get_subpages(l):
    for link in l:
        if not l[link]:
            links_subpages = get_pages(link)
            l[link] = True
        else:
            links_subpages = {}
        l = {**links_subpages, **l}
    return l


@app.post("/taskone", response_model=schemas.Addresses, status_code=status.HTTP_201_CREATED)
def url_registration(url: str = Body(embed=True), db=Depends(get_db)):
    ''' Url registration + data collection'''
    try:
        request = requests.get(url)
        final_url = request.url
        status_code = request.status_code
        title = get_text(request.text, '<title>', '</title>')
        domain_name = get_text(url, "://", '/')

        if final_url == url:
            final_url = ''

        data = {'url': url, "final_url": final_url, "status_code": status_code,
                'domain_name': domain_name, 'title': title}
        db_data = crud.create_address(db, data)
        return db_data
    except (requests.exceptions.InvalidURL, requests.exceptions.InvalidSchema, requests.exceptions.MissingSchema):
        raise HTTPException(status_code=400, detail="URL has some problems")
    except:
        raise HTTPException(status_code=500, detail="We have some problems")


@app.get("/taskone", response_model=list[schemas.Addresses], status_code=status.HTTP_200_OK)
def get_requested_urls(db=Depends(get_db)):
    db_list = crud.get_all_addresses(db)
    return db_list


@app.post("/tasktwo", response_model=schemas.Pages, status_code=status.HTTP_200_OK)
def get_statistic(url: str = Body(embed=True), db=Depends(get_db)):
    '''Obtaining statistics by domain. Domain should end with "/".'''

    if not url.endswith('/'):
        raise HTTPException(status_code=400, detail='Url should end with / ')
    try:
        dict_links = {url: False}
        counter = None
        while counter != 0:
            dict_links2 = get_subpages(dict_links)
            counter = sum(value == False for value in dict_links2.values())
            dict_links = dict_links2

        page_count = len(dict_links)
        active_page_count = sum(value == True for value in dict_links.values())
        url_list = [link for link in dict_links.keys()]
        return {"page_count": page_count, "active_page_count": active_page_count, "url_list": url_list}
    except (requests.exceptions.InvalidURL, requests.exceptions.InvalidSchema, requests.exceptions.MissingSchema):
        raise HTTPException(status_code=400, detail="URL has some problems")
    except (requests.exceptions.TooManyRedirects):
        raise HTTPException(
            status_code=400, detail="There are too many redirects")
    except:
        raise HTTPException(status_code=500, detail="We have some problems")
